# Вставляйте решение под задачей

# Пример:
# Создайте переменную с именем carname и присвойте ей значение Volvo.
carname = 'volvo'


# Выведите переменную функцией print
# --- > Здесь опишите решение


# Создайте несколько строковых переменных
# --- > Здесь опишите решение


# Сложите строковые переменные
# --- > Здесь опишите решение


# Создайте несколько числовых переменных
# --- > Здесь опишите решение


# Сложите числовые переменные
# --- > Здесь опишите решение


# Вычтите одну числовую переменную из другой
# --- > Здесь опишите решение


# Выведите произведение одной переменной на другую
# --- > Здесь опишите решение


# Выведите деление одной переменной на другую
# --- > Здесь опишите решение


# Попробуйте использовать дробные числа (2.5)
a = 5.5
b = 2.5
c = a - b
print(c)


# Почитайте https://younglinux.info/python/input.php
